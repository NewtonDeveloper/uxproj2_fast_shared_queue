//
// Created by endrju on 24.03.19.
//

#include "prompt.h"
#include <stdio.h>
#include <string.h>
#include "shared_queue.h"
#include <time.h>
#define BILLION 1000000000L

static cmd_t commands[]={
             {"help", CMD_HELP, help_process, "help\n"},
             {"push", CMD_PUSH, push_process, "push <data>\n"},
             {"pop", CMD_POP, pop_process, "pop\n"},
             {"restart", CMD_RESTART, restart_process, "restart\n"},
             {"quit", CMD_QUIT, quit_process, "quit\n"}
   };

static int max_cmd = sizeof(commands)/sizeof(cmd_t);



bool help_process(request_t *req)
{
    printf("list of commands:\n");
    for(int i =0; i<max_cmd; ++i)
    {
        printf("%s", commands[i].description);
    }

    return true;
}

bool push_process(request_t *req)
{
   int length = strlen(req->m_line_buf);
#ifdef MYTIME
   struct timespec start, stop;
   clock_gettime(CLOCK_MONOTONIC, &start);
#endif
   push(req->m_line_buf, length +1);
#ifdef MYTIME
   clock_gettime(CLOCK_MONOTONIC, &stop);
#endif
   printf("write %d bytes\n", length+1);
#ifdef MYTIME
   long long unsigned int diff = BILLION * (stop.tv_sec - start.tv_sec) + stop.tv_nsec - start.tv_nsec;
   printf("[time] %ld us", diff/1000);
#endif
   return true;
}

bool pop_process(request_t *req)
{
	char dest[5*MEGABYTE];
	int size;
#ifdef MYTIME
   struct timespec start, stop;
   clock_gettime(CLOCK_MONOTONIC, &start);
#endif
    size = pop(dest);
#ifdef MYTIME
   clock_gettime(CLOCK_MONOTONIC, &stop);
#endif
    if(size == -1) return false;
    printf("%s\n", dest);
    printf("read %d bytes\n",size);
#ifdef MYTIME
    long long unsigned int diff = BILLION * (stop.tv_sec - start.tv_sec) + stop.tv_nsec - start.tv_nsec;
    printf("[time] = %ld us", diff/1000);
#endif

    return true;
}

bool restart_process(request_t *req)
{
	return restart();
}

bool quit_process(request_t *req)
{
   return false;
}

void prompt()
{
    request_t req;
    char *line_buf = NULL;
    size_t line_buf_size = 0;

    do {
    	printf(">>");
        int chars = getline(&line_buf, &line_buf_size, stdin);
        line_buf[chars-1] = '\0';
        if(line_buf[0] == '\0') continue;
        bool correct = decode_request(&req, line_buf);
        correct = (correct == true ?  process_request(&req) : false);
        if(correct)
        {
           printf("\nDONE\n");
        }
        else if (req.cmd_code == CMD_QUIT)
        {
            printf("\nQUIT\n");
        }
        else
        {
            printf("\nBAD\n");
        }

    }
    while(strcmp(line_buf, "quit") != 0);

}



bool decode_request(request_t *req, char* line_buf)
{
    const char *token = strtok(line_buf, " ");
    line_buf+=strlen(token)+1;
#ifdef DEBUG
        printf("recognized token: %s\n",0,token);
#endif
    for(int i=0; i<max_cmd ; ++i)
    {
       if(strcmp(commands[i].name, token) == 0)
       {
           req->cmd_code = commands[i].cmd_code;
           req->m_line_buf = line_buf;
           return true;
       }
    }
    return false;
}


bool process_request(request_t *req)
{
    return commands[req->cmd_code].process(req);
}

