/*
 * producer_consumer.c
 *
 *  Created on: Jun 1, 2019
 *      Author: endrju
 */
#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdlib.h>


void block_queue()
{
	key_t key = ftok("./UXproj2", 'b');


    int sem_id = semget(key, 1, 0666);

    if(sem_id == -1)
    {
       sem_id = semget(key, 2, 0666 | IPC_CREAT);
       semctl(sem_id,0,SETVAL, 0);
       return;
    }

    sem_id = semget(key, 2, 0666 | IPC_CREAT);
#ifdef DEBUG
    printf("sem ID %d\n", sem_id);
#endif
    int sem1_result = semctl(sem_id, 0, GETVAL);
#ifdef DEBUG
    printf("before value of sem_block = %d\n", sem1_result);
#endif

    struct sembuf operations;
    operations.sem_num = 0;
    operations.sem_op = -1;
    operations.sem_flg = 0;

    int status = semop(sem_id, &operations, 1);
    if(status < 0)
    {
#ifdef DEBUG
        perror("can not semctl");
#endif
    }

    sem1_result = semctl(sem_id, 0, GETVAL);
#ifdef DEBUG
    printf("after value of sem_block = %d\n", sem1_result);
#endif

}

void unblock_queue()
{
	key_t key = ftok("./UXproj2", 'b');


    int sem_id = semget(key, 1, 0666);

    if(sem_id == -1)
    {
       sem_id = semget(key, 2, 0666 | IPC_CREAT);
       semctl(sem_id,0,SETVAL, 1);
       return;
    }

    sem_id = semget(key, 2, 0666 | IPC_CREAT);
#ifdef DEBUG
    printf("sem ID %d\n", sem_id);
#endif
    int sem1_result = semctl(sem_id, 0, GETVAL);
#ifdef DEBUG
    printf("before value of sem_block = %d\n", sem1_result);
#endif

    struct sembuf operations;
    operations.sem_num = 0;
    operations.sem_op = 1;
    operations.sem_flg = 0;

    int status = semop(sem_id, &operations, 1);
    if(status < 0)
    {
#ifdef DEBUG
        perror("can not semctl");
#endif
    }

    sem1_result = semctl(sem_id, 0, GETVAL);
#ifdef DEBUG
    printf("after value of sem_block = %d\n", sem1_result);
#endif

}





void producer_queue(unsigned int queue_size)
{
	key_t key = ftok("./UXproj2", 'a');


    int sem_id = semget(key, 2, 0666);

    if(sem_id == -1)
    {

       sem_id = semget(key, 2, 0666 | IPC_CREAT);
       semctl(sem_id,1,SETVAL, 1);
       semctl(sem_id,0,SETVAL, queue_size -1);
       return;
    }

    sem_id = semget(key, 2, 0666 | IPC_CREAT);
#ifdef DEBUG
    printf("sem ID %d\n", sem_id);
#endif
    int sem1_result = semctl(sem_id, 0, GETVAL);
#ifdef DEBUG
    printf("before value of sem1 = %d\n", sem1_result);
#endif
    int sem2_result = semctl(sem_id, 1, GETVAL);
#ifdef DEBUG
    printf("before value of sem2 = %d\n", sem2_result);
#endif

    struct sembuf operations[2];
    operations[0].sem_num = 0;
    operations[0].sem_op = -1;
    operations[0].sem_flg = 0;
    operations[1].sem_num = 1;
    operations[1].sem_op = 1;
    operations[1].sem_flg = 0;

    int status = semop(sem_id, operations, 2);
    if(status < 0)
    {
#ifdef DEBUG
        perror("can not semctl");
#endif
    }

    sem1_result = semctl(sem_id, 0, GETVAL);
#ifdef DEBUG
    printf("after value of sem1 = %d\n", sem1_result);
#endif
    sem2_result = semctl(sem_id, 1, GETVAL);
#ifdef DEBUG
    printf("after value of sem2 = %d\n", sem2_result);
#endif

}

void consumer_queue(unsigned int queue_size)
{
	key_t key = ftok("./UXproj2", 'a');

    int sem_id = semget(key, 2, 0666);

    if(sem_id == -1)
    {

       sem_id = semget(key, 2, 0666 | IPC_CREAT);
       semctl(sem_id,0,SETVAL, queue_size);
    }



    sem_id = semget(key, 2,IPC_CREAT|0666);
#ifdef DEBUG
    printf("sem ID %d\n", sem_id);
#endif
    int sem1_result = semctl(sem_id, 0, GETVAL);
#ifdef DEBUG
    printf("before value of sem1 = %d\n", sem1_result);
#endif

    int sem2_result = semctl(sem_id, 1, GETVAL);
#ifdef DEBUG
    printf("before value of sem2 = %d\n", sem2_result);
#endif

    struct sembuf operations[2];
    operations[0].sem_num = 0;
    operations[0].sem_op = 1;
    operations[0].sem_flg = 0;
    operations[1].sem_num = 1;
    operations[1].sem_op = -1;
    operations[1].sem_flg = 0;

    int status = semop(sem_id, operations, 2);

    if(status < 0)
    {
#ifdef DEBUG
        perror("can not semctl");
#endif
    }

    sem1_result = semctl(sem_id, 0, GETVAL);
#ifdef DEBUG
    printf("after value of sem1 = %d\n", sem1_result);
#endif

    sem2_result = semctl(sem_id, 1, GETVAL);
#ifdef DEBUG
    printf("after value of sem2 = %d\n", sem2_result);
#endif

}

void sem_restart(int queue_size)
{
	key_t key = ftok("./UXproj2", 'a');


	int sem_id = semget(key, 2, 0666 | IPC_CREAT);
	semctl(sem_id,2, IPC_RMID);
	return;
}

