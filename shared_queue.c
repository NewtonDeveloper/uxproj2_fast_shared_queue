//
// Created by endrju on 26.04.19.
//

#include "shared_queue.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "producer_consumer.h"


void* init_segment(int alloc_size)
{
	 key_t key = ftok("./UXproj2", 'b');
     void *ptr = client_memory(key, alloc_size);
     if( ptr == NULL )
     {
         ptr = serve_memory(key, alloc_size);
         Header header;
         header.elements=0;
         header.first_element=0;

         for(int i=0; i < MAX_ELEM; ++i)
         {
             header.each_offset[i] = -1;
             header.each_size[i] = -1;
         }
         memcpy(ptr, &header, sizeof(header));
     }
     return ptr;
}

bool push(const void* src, int size)
{
	producer_queue(MAX_ELEM);
    block_queue();
    void *shared_segment = init_segment(5 * MEGABYTE);
    Header *header = shared_segment;

    shared_segment += sizeof(Header);
    int cur_elem = header->elements;

    if (cur_elem -header->first_element - 1  >= 0) {
        header->each_offset[cur_elem] = header->each_offset[cur_elem - 1]
                + header->each_size[cur_elem - 1];
        header->each_size[cur_elem] = size;
    }
    else {
        header->each_offset[cur_elem]= 0;
        header->each_size[cur_elem] = size;
    }

    memcpy(shared_segment+header->each_offset[cur_elem], src, size);
#ifdef DEBUG
    printf("save virtual address: %#x\n", shared_segment+header->each_offset[cur_elem]);
    printf("save offset: %#x\n", header->each_offset[cur_elem]);
#endif
    header->elements += 1;
    unblock_queue();
    return true;
}




bool restart()
{
    void *shared_segment = init_segment(5 * MEGABYTE);
    Header *header = shared_segment;
    for(int i = 0; i < MAX_ELEM; ++i)
    {
        header->each_offset[i] = -1;
        header->each_size[i] = -1;
    }
    header->elements=0;
    header->first_element = 0;
    sem_restart(MAX_ELEM);
    return true;
}

/*
void copy_back(int* ptr, int elem_size, int n)
{
    for(int i=0; i < n-1; ++i)
    {
        ptr[i]= ptr[i+1];
    }
}
*/

int pop(void * dest)
{
	consumer_queue(MAX_ELEM);
    block_queue();
    void *shared_segment = init_segment(5 * MEGABYTE);
    Header *header = shared_segment;

    shared_segment += sizeof(Header);
    int cur_elem= header->first_element;
    int size = header->each_size[cur_elem];
    if(header->elements == 0)
    {
        return -1;
    }
#ifdef DEBUG
    printf("load address: %#x\n", shared_segment+header->each_offset[cur_elem]);
    printf("load offset: %#x\n", header->each_offset[cur_elem]);
#endif
    memcpy(dest, shared_segment+header->each_offset[cur_elem], header->each_size[cur_elem]);
    header->each_size[cur_elem] = -1;
    header->each_offset[cur_elem] = -1;
    header->first_element+=1;
#ifdef DEBUG
    printf("header->each_offset[0]=%d\n", header->each_offset[0]);
    printf("header->each_offset[1]=%d\n", header->each_offset[1]);
#endif
/*
    if(header->elements >0) {
        //memcpy(header->each_offset, header->each_offset + sizeof(int), sizeof(int) * header->elements);
        copy_back(header->each_offset, sizeof(int), header->elements+1);
#ifdef DEBUG
        printf("header->each_offset[0]=%d\n", header->each_offset[0]);
#endif
        //memcpy(header->each_size, header->each_size + sizeof(int), sizeof(int) * header->elements);
        copy_back(header->each_size, sizeof(int), header->elements+1);
  }
*/
#ifdef DEBUG
    printf("last %d elements\n", header->elements - header->first_element);
    printf("first_element = %d\n", header->first_element);
    printf("elements = %d\n", header->elements);
#endif
    unblock_queue();
    return size;
}


void* serve_memory(key_t mem_key, int memory_size) {

    int shm_id;
    int *shm_ptr;
    shm_id = shmget(mem_key, memory_size, IPC_CREAT | 0666);
    if (shm_id < 0)
    {
#ifdef DEBUG
        printf("*** shmget error (server) ***\n");
#endif
        exit(1);
    }
    shm_ptr = (int *) shmat(shm_id, NULL, 0); /* attach */
    if (shm_ptr == (void*)-1 )
    {
#ifdef DEBUG
        printf("*** shmat error (server) ***\n");
#endif
        exit(1);
    }
    return shm_ptr;
}


void* client_memory(key_t mem_key, int memory_size){

    int shm_id;
    int *shm_ptr;

    shm_id = shmget(mem_key, memory_size, 0666);
    if (shm_id < 0)
    {
        return NULL;
    }
    shm_ptr = (int *) shmat(shm_id, NULL, 0);
    if (shm_ptr == (void*)-1 )
    { /* attach */
#ifdef DEBUG
        printf("*** shmat error (client) ***\n");
#endif
        exit(1);
    }

    return shm_ptr;

}
