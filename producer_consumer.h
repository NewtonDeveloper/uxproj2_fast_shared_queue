/*
 * producer_consumer.h
 *
 *  Created on: Jun 1, 2019
 *      Author: endrju
 */

#ifndef PRODUCER_CONSUMER_H_
#define PRODUCER_CONSUMER_H_

void producer_queue(unsigned int queue_size);
void consumer_queue(unsigned int queue_size);
void block_queue();
void unblock_queue();

void sem_restart(int queue_size);




#endif /* PRODUCER_CONSUMER_H_ */
