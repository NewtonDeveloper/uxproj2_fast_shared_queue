//
// Created by endrju on 26.04.19.
//

#ifndef UXPROJ2_SHARED_QUEUE_H
#define UXPROJ2_SHARED_QUEUE_H
#include <stdbool.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>

#define MEGABYTE 1000000
#define MAX_ELEM 3
#define LIMITS 1000

void* serve_memory(key_t mem_key, int memory_size);
void* client_memory(key_t mem_key, int memory_size);

typedef struct Header
{
    int elements;
    int first_element;
    int each_offset[LIMITS];
    int each_size[LIMITS];

} Header;





void* init_segment(int alloc_size);
bool push(const void* src, int size);
int pop(void * dest);
bool restart();

#endif //UXPROJ2_SHARED_QUEUE_H
