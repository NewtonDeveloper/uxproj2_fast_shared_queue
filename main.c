#include "shared_queue.h"
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include "prompt.h"
#include "shared_queue.h"


int main() {
    printf("Fast queue in shared memory\n");
    init_segment(5 * MEGABYTE);
    prompt();
    return 0;
}
