//
// Created by endrju on 24.03.19.
//

#ifndef UXPROJI_PROMPT_H
#define UXPROJI_PROMPT_H

#define MAX_ARGC 15

#include <stdbool.h>


#define CMD_HELP 0
#define CMD_PUSH 1
#define CMD_POP 2
#define CMD_RESTART 3
#define CMD_QUIT 4

typedef struct request_t{
    unsigned int m_id;
    unsigned char cmd_code;
    const char * m_line_buf;
} request_t;


typedef struct cmd_t{
    char* name;
    unsigned char cmd_code;
    bool (*process)(request_t*);
    char* description;
} cmd_t;




bool help_process(request_t *req);
bool push_process(request_t *req);
bool pop_process(request_t *req);
bool restart_process(request_t *req);
bool quit_process(request_t *req);


bool decode_request(request_t *req, char* line_buf);
bool process_request(request_t *req);


void prompt();



#endif //UXPROJI_PROMPT_H
